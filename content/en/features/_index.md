
---
title: "Request a Feature"
linkTitle: "Request a Feature"
weight: 20
menu:
  main:
    weight: 20
---



<script type="text/javascript" src="https://pcas.atlassian.net/s/d41d8cd98f00b204e9800998ecf8427e-T/-dtzt95/b/6/c95134bc67d3a521bb3f4331beb9b804/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=en-US&collectorId=e3148c19"></script>

<script type="text/javascript">window.ATL_JQ_PAGE_PROPS =  {
    "triggerFunction": function(showCollectorDialog) {
        jQuery("#feature_button").click(function(e) {
            e.preventDefault();
            showCollectorDialog();
        });
    }};</script>


{{< blocks/section color="white">}} 

<a href="#" id="feature_button" class='btn btn-primary btn-large'>Request a feature</a>
<a href="https://pcas.atlassian.net/jira/dashboards/10003" class='btn btn-primary btn-large'>View open issues</a>

{{< /blocks/section >}}





---

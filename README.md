# This repository

This repository contains scripts and data related to the website for the [PCAS](https://www.pcas.xyz) project.

## Notes on installation

After cloning this repository, you need to add [Docsy](https://github.com/google/docsy) as a git submodule:

```bash
▶ git submodule
 a053131a4ebf6a59e4e8834a42368e248d98c01d themes/docsy (heads/master)
```

You should then pull in local copies of the submodules. To do so, move to the root directory of the repository and do:

```bash
git submodule update --init --recursive
```

## License and Copyright

This work is based on the [Example Project](https://example.docsy.dev/) provided as part of [Docsy](https://github.com/google/docsy); it includes Docsy as a git submodule.  Docsy and the Example Project were released under the Apache License, Version 2.0. 

This work also includes the image `content/en/featured-background.jpg` by [Gemma Anderson](http://www.gemma-anderson.co.uk); that image is copyright Gemma Anderson, and used with permission. 

Everything else in this repository is copyright 2021 Tom Coates and Alexander Kasprzyk, and released under the Apache License, Version 2.0.

## Apache License

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
